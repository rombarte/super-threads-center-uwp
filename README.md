# Super Threads Center UWP
Simple discussion board written in C# for UWP platform - version for Windows 10

## License
All files in this repository are available under the MIT license. You can find more information and a link to the license content on the [Wikipedia](https://en.wikipedia.org/wiki/MIT_License) page.